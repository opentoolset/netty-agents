// ---
// Copyright 2020 netty-agents team
// All rights reserved
// ---
package org.opentoolset.nettyagents;

import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.opentoolset.nettyagents.MessageFixture.SampleMessage;
import org.opentoolset.nettyagents.MessageFixture.SampleRequest;
import org.opentoolset.nettyagents.MessageFixture.SampleResponse;
import org.opentoolset.nettyagents.agents.ClientAgent;
import org.opentoolset.nettyagents.agents.ServerAgent;

@Slf4j
public class ITMultiClient {

	private static ServerAgent serverAgent;
	private static ClientAgent clientAgent1;
	private static ClientAgent clientAgent2;

	// ---

	@Test
	public void test() {
		serverAgent.startup();
		clientAgent1.startup();
		clientAgent2.startup();

		Utils.waitUntil(() -> serverAgent.getClients().size() > 1, 600);

		Map<String, PeerContext> clientsById = serverAgent.getClientsById();
		PeerContext client1 = clientsById.get(clientAgent1.getConfig().getId());
		PeerContext client2 = clientsById.get(clientAgent2.getConfig().getId());

		{
			clientAgent1.sendMessage(new SampleMessage("Sample message from client-1"));
			clientAgent2.sendMessage(new SampleMessage("Sample message from client-2"));

			{
				SampleResponse response = clientAgent1.doRequest(new SampleRequest("Sample request from client-1", 2));
				log.info("Response received from server: {}", response);
				Assert.assertNotNull(response);
				Assert.assertEquals(4, response.getNumber());
			}

			{
				SampleResponse response = clientAgent2.doRequest(new SampleRequest("Sample request from client-2", 3));
				log.info("Response received from server: {}", response);
				Assert.assertNotNull(response);
				Assert.assertEquals(9, response.getNumber());
			}
		}

		{
			serverAgent.sendMessage(new SampleMessage("Sample message from server to client1"), client1);
			serverAgent.sendMessage(new SampleMessage("Sample message from server to client2"), client2);

			{
				SampleResponse response = serverAgent.doRequest(new SampleRequest("Sample request from server to client-1", 1), client1);
				log.info("Response received from client: {}", response);
				Assert.assertNotNull(response);
				Assert.assertEquals(2, response.getNumber());
			}

			{
				SampleResponse response = serverAgent.doRequest(new SampleRequest("Sample request from server to client-2", 3), client2);
				log.info("Response received from client: {}", response);
				Assert.assertNotNull(response);
				Assert.assertEquals(5, response.getNumber());
			}
		}

		clientAgent1.shutdown();
		clientAgent2.shutdown();
		serverAgent.shutdown();
	}

	// ---

	@BeforeClass
	public static void beforeClass() {
		serverAgent = new ServerAgent();
		serverAgent.setMessageHandler(SampleMessage.class, message -> handleMessageOnServer(message));
		serverAgent.setRequestHandler(SampleRequest.class, request -> handleRequestOnServer(request));

		clientAgent1 = new ClientAgent();
		clientAgent1.setMessageHandler(SampleMessage.class, message -> handleMessageOnClient1(message));
		clientAgent1.setRequestHandler(SampleRequest.class, request -> handleRequestOnClient1(request));

		clientAgent2 = new ClientAgent();
		clientAgent2.setMessageHandler(SampleMessage.class, message -> handleMessageOnClient2(message));
		clientAgent2.setRequestHandler(SampleRequest.class, request -> handleRequestOnClient2(request));
	}

	private static void handleMessageOnServer(SampleMessage message) {
		log.info("Message received on server: {}", message);
	}

	private static SampleResponse handleRequestOnServer(SampleRequest request) {
		log.info("Request received on server: {}", request);
		SampleResponse response = new SampleResponse("Sample response from server (square)", (int) Math.pow(request.getNumber(), 2));
		log.info("Response sending on server: {}", response);
		return response;
	}

	private static void handleMessageOnClient1(SampleMessage message) {
		log.info("Message received on client1: {}", message);
	}

	private static SampleResponse handleRequestOnClient1(SampleRequest request) {
		log.info("Request received on client1: {}", request);
		SampleResponse response = new SampleResponse("Sample response from client1 (increment by 1)", request.getNumber() + 1);
		log.info("Response sending on client1: {}", response);
		return response;
	}

	private static void handleMessageOnClient2(SampleMessage message) {
		log.info("Message received on client2: {}", message);
	}

	private static SampleResponse handleRequestOnClient2(SampleRequest request) {
		log.info("Request received on client2: {}", request);
		SampleResponse response = new SampleResponse("Sample response from client2 (increment by 2)", request.getNumber() + 2);
		log.info("Response sending on client2: {}", response);
		return response;
	}
}
