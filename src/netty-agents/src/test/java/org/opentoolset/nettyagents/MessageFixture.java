// ---
// Copyright 2020 netty-agents team
// All rights reserved
// ---
package org.opentoolset.nettyagents;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public interface MessageFixture {

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class SampleRequest1 extends AbstractRequest<SampleResponse1> {

		private String text;
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class SampleRequest2 extends AbstractRequest<SampleResponse2> {

		private String text;
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class SampleRequest3 extends AbstractRequest<SampleResponse3> {

		private String text;
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class SampleResponse1 extends AbstractMessage {

		private int number;
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class SampleResponse2 extends AbstractMessage {

		private int number;
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class SampleResponse3 extends AbstractMessage {

		private int number;
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class SampleMessage extends AbstractMessage {

		private String text;
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class SampleRequest extends AbstractRequest<SampleResponse> {

		private String text;
		private int number;
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class SampleResponse extends AbstractMessage {

		private String text;
		private int number;
	}
}
