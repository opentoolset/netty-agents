// ---
// Copyright 2020 netty-agents team
// All rights reserved
// ---
package org.opentoolset.nettyagents;

import io.netty.handler.ssl.util.SelfSignedCertificate;
import java.net.SocketAddress;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.opentoolset.nettyagents.MessageFixture.SampleMessage;
import org.opentoolset.nettyagents.MessageFixture.SampleRequest;
import org.opentoolset.nettyagents.MessageFixture.SampleRequest1;
import org.opentoolset.nettyagents.MessageFixture.SampleRequest2;
import org.opentoolset.nettyagents.MessageFixture.SampleRequest3;
import org.opentoolset.nettyagents.MessageFixture.SampleResponse;
import org.opentoolset.nettyagents.MessageFixture.SampleResponse1;
import org.opentoolset.nettyagents.MessageFixture.SampleResponse2;
import org.opentoolset.nettyagents.MessageFixture.SampleResponse3;
import org.opentoolset.nettyagents.agents.ClientAgent;
import org.opentoolset.nettyagents.agents.ServerAgent;

@Slf4j
public class ITNettyAgents {

	@Test
	public void testAgentOperationsTwoWay() {
		ServerAgent serverAgent = new ServerAgent();
		ClientAgent clientAgent = new ClientAgent();

		startAgents(serverAgent, clientAgent);
		testAgentOperations(serverAgent, clientAgent);
	}

	@Test
	public void testOneWaySyncAndAsyncTogetherUsingObserver() {
		ServerAgent serverAgent = new ServerAgent();
		ClientAgent clientAgent = new ClientAgent();

		startAgents(serverAgent, clientAgent);
		testAgentOperationsUsingObserver(serverAgent, clientAgent);
	}

	@Test
	public void testOneWaySyncAndAsyncTogetherUsingFuture() {
		ServerAgent serverAgent = new ServerAgent();
		ClientAgent clientAgent = new ClientAgent();

		startAgents(serverAgent, clientAgent);
		testAgentOperationsUsingFuture(serverAgent, clientAgent);
	}

	@Test
	public void testWithTLS() {
		System.out.println("Testing with TLS ...");

		ServerAgent serverAgent = new ServerAgent();
		ClientAgent clientAgent = new ClientAgent();

		configureTLS(serverAgent, clientAgent);

		{
			X509Certificate serverCert = serverAgent.getConfig().getTlsConfig().getCert();
			String serverFingerprint = Utils.getFingerprintAsHex(serverCert);
			clientAgent.getContext().getTrustedCerts().put(serverFingerprint, serverCert);
		}

		{
			X509Certificate clientCert = clientAgent.getConfig().getTlsConfig().getCert();
			String clientFingerprint = Utils.getFingerprintAsHex(clientCert);
			serverAgent.getContext().getTrustedCerts().put(clientFingerprint, clientCert);
		}

		startAgents(serverAgent, clientAgent);
		testAgentOperations(serverAgent, clientAgent);
	}

	@Test
	public void testWithTLSAndPeerIdentificationMode() throws Exception {
		System.out.println("Testing with TLS and peer identification mode...");

		ServerAgent serverAgent = new ServerAgent();
		ClientAgent clientAgent = new ClientAgent();

		configureTLS(serverAgent, clientAgent);

		serverAgent.startPeerIdentificationMode();
		clientAgent.startPeerIdentificationMode();

		startAgents(serverAgent, clientAgent);

		for (Entry<SocketAddress, PeerContext> entry : serverAgent.getClients().entrySet()) {
			SocketAddress socketAddress = entry.getKey();
			PeerContext client = entry.getValue();

			X509Certificate clientCert = client.getCert();
			String clientFingerprint = Utils.getFingerprintAsHex(clientCert);
			log.info("Remote socket: {} - Client fingerprint: {}", socketAddress, clientFingerprint);

			{
				// if user on server side trusts the client after checking client fingerprint via a seperate channel (ie. offline methods)
				client.setTrusted(true);

				// if user on server side decides to save this trust in memory
				serverAgent.getContext().getTrustedCerts().put(clientFingerprint, clientCert);
			}
		}

		X509Certificate serverCert = clientAgent.getServer().getCert();
		String serverFingerprint = Utils.getFingerprintAsHex(serverCert);
		log.info("Server fingerprint: {}", serverFingerprint);

		{
			// if user on client side trusts the client after checking client fingerprint via a seperate channel (ie. offline methods)
			clientAgent.getServer().setTrusted(true);

			// if user on client side decides to save this trust in memory
			clientAgent.getContext().getTrustedCerts().put(serverFingerprint, serverCert);
		}

		serverAgent.stopPeerIdentificationMode();
		clientAgent.stopPeerIdentificationMode();

		testAgentOperations(serverAgent, clientAgent);
	}

	@SneakyThrows
	private void configureTLS(ServerAgent serverAgent, ClientAgent clientAgent) {
		{ // --- on the server side
			SelfSignedCertificate cert = new SelfSignedCertificate();
			String serverPriKey = Utils.base64Encode(cert.key().getEncoded());
			String serverCert = Utils.base64Encode(cert.cert().getEncoded());

			serverAgent.getConfig().getTlsConfig().setEnabled(true);
			serverAgent.getConfig().getTlsConfig().setPriKey(serverPriKey);
			serverAgent.getConfig().getTlsConfig().setCert(serverCert);
		}

		{ // --- on the client side
			SelfSignedCertificate cert = new SelfSignedCertificate();
			String clientPriKey = Utils.base64Encode(cert.key().getEncoded());
			String clientCert = Utils.base64Encode(cert.cert().getEncoded());

			clientAgent.getConfig().getTlsConfig().setEnabled(true);
			clientAgent.getConfig().getTlsConfig().setPriKey(clientPriKey);
			clientAgent.getConfig().getTlsConfig().setCert(clientCert);
		}
	}

	// ---

	private void testAgentOperations(ServerAgent serverAgent, ClientAgent clientAgent) {
		Utils.waitUntil(() -> !serverAgent.getClients().isEmpty(), 1000);
		Utils.waitUntil(() -> clientAgent.getServer().getChannelHandlerContext() != null, 1000);

		PeerContext client = serverAgent.getClients().values().iterator().next();
		log.info("Client: {}", client);

		{
			// --- client to server:

			clientAgent.sendMessage(new SampleMessage("Sample message to server"));

			SampleResponse response = clientAgent.doRequest(new SampleRequest("Sample request to server", 1));
			log.info("Response received from server: {}", response);
			Assert.assertNotNull(response);
			Assert.assertEquals(2, response.getNumber());
		}

		{
			// --- server to client:

			serverAgent.sendMessage(new SampleMessage("Sample message to client"), client);

			SampleResponse response = serverAgent.doRequest(new SampleRequest("Sample request to client", 4), client);
			log.info("Response received from client: {}", response);
			Assert.assertNotNull(response);
			Assert.assertEquals(3, response.getNumber());
		}

		clientAgent.shutdown();
		serverAgent.shutdown();

		Utils.waitFor(1);
	}

	@SneakyThrows
	private void testAgentOperationsUsingObserver(ServerAgent serverAgent, ClientAgent clientAgent) {
		PeerContext client = serverAgent.getClients().values().iterator().next();
		log.info("TestWithObserver: Client: {}", client);

		Map<Integer, Boolean> requestCompletionStatuses = new HashMap<>();

		requestCompletionStatuses.put(1, false);
		log.info("TestWithObserver: Request1 is sending asynchronously");
		clientAgent.doRequestAsync(new SampleRequest1("request-1"),
								   resp -> {
									   log.info("TestWithObserver: Request1 is completed. Response: {}", resp);
									   requestCompletionStatuses.put(1, true);
								   });

		requestCompletionStatuses.put(2, false);
		log.info("TestWithObserver: Request2 is sending asynchronously");
		clientAgent.doRequestAsync(new SampleRequest2("request-2"),
								   resp -> {
									   log.info("TestWithObserver: Request2 is completed. Response: {}", resp);
									   requestCompletionStatuses.put(2, true);
								   });

		log.info("TestWithObserver: Request3 is sending synchronously");
		SampleResponse3 response = clientAgent.doRequest(new SampleRequest3("request-3"));
		log.info("TestWithObserver: Request3 is completed. Response: {}", response);

		Utils.waitUntil(() -> requestCompletionStatuses.values().stream().allMatch(Boolean::booleanValue), 600);

		clientAgent.shutdown();
		serverAgent.shutdown();
	}

	@SneakyThrows
	private void testAgentOperationsUsingFuture(ServerAgent serverAgent, ClientAgent clientAgent) {
		PeerContext client = serverAgent.getClients().values().iterator().next();
		log.info("TestWithFuture: Client: {}", client);

		log.info("TestWithFuture: Request1 is sending asynchronously");
		Future<SampleResponse1> future1 = clientAgent.doRequestAsync(new SampleRequest1("request-1"));

		log.info("TestWithFuture: Request2 is sending asynchronously");
		Future<SampleResponse2> future2 = clientAgent.doRequestAsync(new SampleRequest2("request-2"));

		log.info("TestWithFuture: Request3 is sending synchronously");
		SampleResponse3 response = clientAgent.doRequest(new SampleRequest3("request-3"));
		log.info("TestWithFuture: Request3 is completed. Response: {}", response);

		boolean request1Waiting = true;
		boolean request2Waiting = true;
		while (request1Waiting || request2Waiting) {
			if (request1Waiting && future1.isDone()) {
				log.info("TestWithFuture: Request1 is completed. Response: {}", future1.get());
				request1Waiting = false;
			}

			if (request2Waiting && future2.isDone()) {
				log.info("TestWithFuture: Request2 is completed. Response: {}", future2.get());
				request2Waiting = false;
			}

			Utils.waitFor(100, TimeUnit.MILLISECONDS);
		}

		Utils.waitUntil(() -> future1.isDone() && future2.isDone(), 10);

		clientAgent.shutdown();
		serverAgent.shutdown();
	}

	private void startAgents(ServerAgent serverAgent, ClientAgent clientAgent) {
		{ // --- on the server side
			serverAgent.setMessageHandler(SampleMessage.class, this::handleMessageOnServer);
			serverAgent.setRequestHandler(SampleRequest.class, this::handleRequestOnServer);
			serverAgent.setRequestHandler(SampleRequest1.class, this::handleRequest1);
			serverAgent.setRequestHandler(SampleRequest2.class, this::handleRequest2);
			serverAgent.setRequestHandler(SampleRequest3.class, this::handleRequest3);
			serverAgent.startup();
		}

		{ // --- on the client side
			clientAgent.setMessageHandler(SampleMessage.class, this::handleMessageOnClient);
			clientAgent.setRequestHandler(SampleRequest.class, this::handleRequestOnClient);
			clientAgent.startup();
		}

		// --- wait on each side until establishing connections
		Utils.waitUntil(() -> !serverAgent.getClients().isEmpty(), 1);
		Utils.waitUntil(() -> clientAgent.getServer().getChannelHandlerContext() != null, 1);
	}

	// ---

	private void handleMessageOnServer(SampleMessage message) {
		log.info("Message received on server: {}", message);
	}

	private SampleResponse handleRequestOnServer(SampleRequest request) {
		log.info("Request received on server: {}", request);
		SampleResponse response = new SampleResponse("Sample response from server (increment by 1)", request.getNumber() + 1);
		log.info("Response sending on server: {}", response);
		return response;
	}

	private void handleMessageOnClient(SampleMessage message) {
		log.info("Message received on client: {}}", message);
	}

	private SampleResponse handleRequestOnClient(SampleRequest request) {
		log.info("Request received on client: {}", request);
		SampleResponse response = new SampleResponse("Sample response from client (decrement by 1)", request.getNumber() - 1);
		log.info("Response sending on client: {}", response);
		return response;
	}

	@SneakyThrows
	private SampleResponse1 handleRequest1(SampleRequest1 request) {
		log.info("Server: Request1 is received on server and being processed for 5 seconds. Request: {}", request);
		Utils.waitFor(5);

		SampleResponse1 response = new SampleResponse1(1);
		log.info("Server: Request1 is being responded. Response: {}", response);
		return response;
	}

	@SneakyThrows
	private SampleResponse2 handleRequest2(SampleRequest2 request) {
		log.info("Server: Request2 is received on server and being processed for 3 seconds. Request: {}", request);
		Utils.waitFor(3);

		SampleResponse2 response = new SampleResponse2(2);
		log.info("Server: Request2 is being responded. Response: {}", response);
		return response;
	}

	@SneakyThrows
	private SampleResponse3 handleRequest3(SampleRequest3 request) {
		log.info("Server: Request3 is received on server and being processed for 1 second. Request: {}", request);
		Utils.waitFor(1);

		SampleResponse3 response = new SampleResponse3(3);
		log.info("Server: Request3 is being responded. Response: {}", response);
		return response;
	}
}
