package org.opentoolset.nettyagents;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.opentoolset.nettyagents.MessageWrapper.Serializer;

@Slf4j
public class SerializerJson implements Serializer {

	@Override
	public String serialize(Object obj) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
			return mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			log.error(e.getLocalizedMessage(), e);
			return null;
		}
	}

	@Override
	public <T> T deserialize(String serialized, Class<T> classOfObj) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			return mapper.readValue(serialized, classOfObj);
		} catch (IOException e) {
			log.error(e.getLocalizedMessage(), e);
			return null;
		}
	}
}
