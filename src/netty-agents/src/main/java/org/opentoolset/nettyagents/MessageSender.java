// ---
// Copyright 2020 netty-agents team
// All rights reserved
// ---
package org.opentoolset.nettyagents;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MessageSender {

	private Map<String, OperationContext> waitingRequests = new ConcurrentHashMap<>();

	private Context context;

	// ---

	public MessageSender(Context context) {
		this.context = context;
	}

	public <O extends AbstractRequest<I>, I extends AbstractMessage> I doRequest(O request, PeerContext peerContext) {
		if (this.context.isTrustNegotiationMode()) {
			return null;
		}

		return doRequest(request, peerContext, Constants.DEFAULT_REQUEST_TIMEOUT_SEC);
	}

	public <I extends AbstractRequest<O>, O extends AbstractMessage> O doRequest(I request, PeerContext peerContext, int timeoutSec) {
		if (this.context.isTrustNegotiationMode()) {
			return null;
		}

		try {
			if (!Utils.waitUntil(() -> peerContext.getChannelHandlerContext() != null, Constants.DEFAULT_CHANNEL_WAIT_SEC)) {
				return null;
			}

			OperationContext operationContext = OperationContext.builder().thread(Thread.currentThread()).build();
			MessageWrapper requestWrapper = MessageWrapper.createRequest(request);
			this.waitingRequests.put(requestWrapper.getId(), operationContext);

			peerContext.getChannelHandlerContext().writeAndFlush(requestWrapper);
			synchronized (operationContext.getThread()) {
				operationContext.getThread().wait(timeoutSec * 1000L);
			}

			operationContext = this.waitingRequests.remove(requestWrapper.getId());
			if (operationContext != null) {
				MessageWrapper responseWrapper = operationContext.getResponseWrapper();
				if (responseWrapper != null) {
					return responseWrapper.deserializeMessage(request.getResponseClass());
				}
			}
		} catch (InterruptedException e) {
			log.error("Interrupted", e);
			Thread.currentThread().interrupt();
		}

		return null;
	}

	public <I extends AbstractRequest<O>, O extends AbstractMessage> void doRequestAsync(I request, PeerContext peerContext, Consumer<O> observer) {
		if (this.context.isTrustNegotiationMode()) {
			return;
		}

		if (!Utils.waitUntil(() -> peerContext.getChannelHandlerContext() != null, Constants.DEFAULT_CHANNEL_WAIT_SEC)) {
			return;
		}

		new Thread(() -> {
			OperationContext operationContext = OperationContext.builder().thread(Thread.currentThread()).build();
			MessageWrapper requestWrapper = MessageWrapper.createRequest(request);
			this.waitingRequests.put(requestWrapper.getId(), operationContext);

			peerContext.getChannelHandlerContext().writeAndFlush(requestWrapper);
			synchronized (operationContext.getThread()) {
				try {
					operationContext.getThread().wait();
				} catch (InterruptedException e) {
					log.error("Interrupted", e);
					operationContext.getThread().interrupt();
				}
			}

			operationContext = this.waitingRequests.remove(requestWrapper.getId());
			if (operationContext != null) {
				MessageWrapper responseWrapper = operationContext.getResponseWrapper();
				if (responseWrapper != null) {
					O responseMessage = responseWrapper.deserializeMessage(request.getResponseClass());
					observer.accept(responseMessage);
				}
			}
		}).start();
	}

	public <I extends AbstractRequest<O>, O extends AbstractMessage> Future<O> doRequestAsync(I request, PeerContext peerContext) {
		CompletableFuture<O> future = new CompletableFuture<>();

		if (this.context.isTrustNegotiationMode()) {
			future.complete(null);
			return future;
		}

		if (!Utils.waitUntil(() -> peerContext.getChannelHandlerContext() != null, Constants.DEFAULT_CHANNEL_WAIT_SEC)) {
			future.completeExceptionally(new TimeoutException("Socket connection timed out"));
			return future;
		}

		new Thread(() -> {
			OperationContext operationContext = OperationContext.builder().thread(Thread.currentThread()).build();
			MessageWrapper requestWrapper = MessageWrapper.createRequest(request);
			this.waitingRequests.put(requestWrapper.getId(), operationContext);

			peerContext.getChannelHandlerContext().writeAndFlush(requestWrapper);
			synchronized (operationContext.getThread()) {
				try {
					operationContext.getThread().wait();
				} catch (InterruptedException e) {
					log.error("Interrupted", e);
					operationContext.getThread().interrupt();
				}
			}

			operationContext = this.waitingRequests.remove(requestWrapper.getId());
			if (operationContext != null) {
				MessageWrapper responseWrapper = operationContext.getResponseWrapper();
				if (responseWrapper != null) {
					O responseMessage = responseWrapper.deserializeMessage(request.getResponseClass());
					future.complete(responseMessage);
				}
			}
		}).start();

		return future;
	}

	public <T extends AbstractMessage> boolean sendMessage(T message, PeerContext peerContext) {
		if (this.context.isTrustNegotiationMode()) {
			return false;
		}

		if (Utils.waitUntil(() -> peerContext.getChannelHandlerContext() != null, Constants.DEFAULT_CHANNEL_WAIT_SEC)) {
			try {
				MessageWrapper messageWrapper = MessageWrapper.create(message);
				peerContext.getChannelHandlerContext().writeAndFlush(messageWrapper);
				return true;
			} catch (Exception e) {
				log.error(e.getLocalizedMessage(), e);
			}
		}
		return false;
	}

	public void shutdown() {
		for (OperationContext operationContext : this.waitingRequests.values()) {
			Thread thread = operationContext.getThread();
			synchronized (thread) {
				thread.notifyAll();
			}
		}
	}

	// ---

	Map<String, OperationContext> getWaitingRequests() {
		return waitingRequests;
	}

	// ---

	@Data
	@Builder
	public static class OperationContext {

		private Thread thread;
		private MessageWrapper responseWrapper;
	}
}