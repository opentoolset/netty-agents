// ---
// Copyright 2020 netty-agents team
// All rights reserved
// ---
package org.opentoolset.nettyagents;

import io.netty.channel.ChannelHandlerContext;
import java.io.ByteArrayInputStream;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXCertPathValidatorResult;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;

@Slf4j
public class Utils {

	public static final String ALGORITHM_RSA = "RSA";
	public static final String CERTIFICATE_TYPE_X509 = "X.509";

	private Utils() {
		throw new IllegalStateException("Utility class");
	}

	public static void waitFor(int duration) {
		waitFor(duration, TimeUnit.SECONDS);
	}

	public static void waitFor(int duration, TimeUnit timeUnit) {
		try {
			timeUnit.sleep(duration);
		} catch (InterruptedException e) {
			log.warn(e.getLocalizedMessage(), e);
			Thread.currentThread().interrupt();
		}
	}

	public static boolean waitUntil(BooleanSupplier until, int timeout) {
		return waitUntil(until, timeout, TimeUnit.SECONDS);
	}

	public static boolean waitUntil(BooleanSupplier until, int timeout, TimeUnit timeUnit) {
		try {
			for (int i = 0; i < timeout; i++) {
				if (until.getAsBoolean()) {
					break;
				}
				timeUnit.sleep(1);
			}
		} catch (InterruptedException e) {
			log.warn(e.getLocalizedMessage(), e);
			Thread.currentThread().interrupt();
		}

		return until.getAsBoolean();
	}

	public static void verifyCertChain(Certificate[] peerCertChain, Map<String, X509Certificate> trustedCerts) throws CertificateException {
		CertificateFactory certFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE_X509);
		Certificate cert = peerCertChain[0];
		CertPath certPath = certFactory.generateCertPath(Arrays.asList(cert));

		try {
			CertPathValidator certPathValidator = CertPathValidator.getInstance("PKIX");

			String fingerprint = getFingerprintAsHex(cert);
			X509Certificate trustedCert = trustedCerts.get(fingerprint);
			if (trustedCert != null) {
				Set<TrustAnchor> trustAnchors = new HashSet<>();
				TrustAnchor trustAnchor = new TrustAnchor(trustedCert, null);
				trustAnchors.add(trustAnchor);

				PKIXParameters params = new PKIXParameters(trustAnchors);
				params.setRevocationEnabled(false);

				PKIXCertPathValidatorResult result = (PKIXCertPathValidatorResult) certPathValidator.validate(certPath, params);
				PublicKey publicKey = result.getPublicKey();
				log.info("Certificate verified. Public key: {}", publicKey);
				return;
			}
		} catch (NoSuchAlgorithmException e) {
			log.error(e.getLocalizedMessage(), e);
		} catch (GeneralSecurityException e) {
			log.debug(e.getLocalizedMessage(), e);
		}

		throw new CertificateException("Certificate couldn't be verified by custom trust manager...");
	}

	public static PrivateKey buildPriKey(String priKeyStr) throws NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] priKeyBytes = Utils.base64Decode(priKeyStr);
		return buildPriKey(priKeyBytes);
	}

	public static RSAPrivateKey buildPriKey(byte[] priKeyBytes) throws NoSuchAlgorithmException, InvalidKeySpecException {
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(priKeyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM_RSA);
		return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
	}

	public static X509Certificate buildCert(String peerCertStr) throws CertificateException {
		byte[] peerCertBytes = Utils.base64Decode(peerCertStr);
		return buildCert(peerCertBytes);
	}

	public static X509Certificate buildCert(byte[] peerCertBytes) throws CertificateException {
		CertificateFactory certificateFactory = CertificateFactory.getInstance(CERTIFICATE_TYPE_X509);
		ByteArrayInputStream inputStream = new ByteArrayInputStream(peerCertBytes);
		return (X509Certificate) certificateFactory.generateCertificate(inputStream);
	}

	public static String getFingerprintAsHex(Certificate cert) {
		byte[] fingerPrintBytes = getFingerprint(cert);
		return Hex.encodeHexString(fingerPrintBytes);
	}

	public static byte[] getFingerprint(Certificate cert) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.reset();
			return md.digest(cert.getEncoded());
		} catch (NoSuchAlgorithmException | CertificateEncodingException e) {
			return new byte[0];
		}
	}

	public static String base64Encode(byte[] bytes) {
		return Base64.getEncoder().encodeToString(bytes);
	}

	public static byte[] base64Decode(String str) {
		return Base64.getDecoder().decode(str);
	}

	public static boolean verifyChannelHandlerContext(ChannelHandlerContext ctx, PeerContext peer) {
		return ctxBelongsToATrustedPeer(ctx, peer);
	}

	public static boolean ctxBelongsToATrustedPeer(ChannelHandlerContext ctx, PeerContext peer) {
		return ctx != null
			&& ctx.channel() != null
			&& peer != null
			&& peer.isTrusted()
			&& peer.getChannelHandlerContext() != null
			&& Objects.equals(ctx.channel(), peer.getChannelHandlerContext().channel());
	}
}
