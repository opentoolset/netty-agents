// ---
// Copyright 2020 netty-agents team
// All rights reserved
// ---
package org.opentoolset.nettyagents;

import io.netty.handler.ssl.SslContext;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import javax.net.ssl.X509TrustManager;
import lombok.Getter;
import lombok.Setter;

/**
 * Abstract Agent class contains common members for its childs ie. Client and Server Agents.
 *
 * @author hadi
 */
public abstract class AbstractAgent {

	private Context context = new Context();

	private SslContext sslContext;

	// ---

	/**
	 * Configuration object including configuration parameters for this agent.<br />
	 * Configuration parameters can be changed if needed. <br />
	 * All configuration adjustments should be made before calling the method "startup".
	 *
	 * @return Configuration object
	 */
	protected abstract AbstractConfig getConfig();

	// ---

	/**
	 * Creates a request handler for a specific request type
	 *
	 * @param classOfRequest Specifies the request type
	 * @param function       Specifies the function which will be executed when this request is made
	 * @param <I>
	 * @param <O>
	 */
	public <I extends AbstractRequest<O>, O extends AbstractMessage> void setRequestHandler(Class<I> classOfRequest,
																							Function<I, O> function) {
		this.context.getMessageReceiver().setRequestHandler(classOfRequest, function);
	}

	/**
	 * Creates a request handler for a specific message type
	 *
	 * @param classOfMessage Specifies the message type
	 * @param consumer       Specifies the consumer which will accept and process this message when it reaches to this agent
	 * @param <T>
	 */
	public <T extends AbstractMessage> void setMessageHandler(Class<T> classOfMessage, Consumer<T> consumer) {
		this.context.getMessageReceiver().setMessageHandler(classOfMessage, consumer);
	}

	/**
	 * Starts peer identification mode. In this mode, it is only allowed to exchange certificates between peers. No other communication is allowed. Any peer may give trust to other peers in this mode if they are authentic.
	 */
	public void startPeerIdentificationMode() {
		this.context.setTrustNegotiationMode(true);
	}

	/**
	 * Ends peer identification mod. After ending this mode, peers can communicate with each other.
	 */
	public void stopPeerIdentificationMode() {
		this.context.setTrustNegotiationMode(false);
	}

	// ---

	/**
	 * Returns context object holding several state variables during the life-cycle of this agent
	 *
	 * @return
	 */
	public Context getContext() {
		return context;
	}

	/**
	 * Starts the agent up ie. by entering listening mode (for server) or making connection attempts to configured server-peer
	 */
	protected void startup() {
	}

	/**
	 * Returns SSL context object for using in SSL related operations
	 *
	 * @return
	 */
	protected SslContext getSslContext() {
		return sslContext;
	}

	/**
	 * Sets SSL context object for using in SSL related operations
	 *
	 * @return
	 */
	protected void setSslContext(SslContext sslContext) {
		this.sslContext = sslContext;
	}

	// ---

	@Getter
	@Setter
	public static class AbstractConfig {

		private TlsConfig tlsConfig = new TlsConfig();
	}

	@Getter
	@Setter
	public static class TlsConfig {

		private boolean enabled = Constants.DEFAULT_TLS_ENABLED;
		private PrivateKey priKey;
		private X509Certificate cert;

		public TlsConfig setPriKey(String priKeyStr) throws NoSuchAlgorithmException, InvalidKeySpecException {
			priKey = Utils.buildPriKey(priKeyStr);
			return this;
		}

		public TlsConfig setCert(String certStr) throws CertificateException {
			cert = Utils.buildCert(certStr);
			return this;
		}
	}

	// ---

	public static class TrustManager implements X509TrustManager {

		private Supplier<Context> contextProvider;

		public TrustManager(Supplier<Context> contextProvider) {
			this.contextProvider = contextProvider;
		}

		@Override
		public void checkClientTrusted(X509Certificate[] peerCertChain, String authType) throws CertificateException {
			if (!contextProvider.get().isTrustNegotiationMode()) {
				Utils.verifyCertChain(peerCertChain, contextProvider.get().getTrustedCerts());
			}
		}

		@Override
		public void checkServerTrusted(X509Certificate[] peerCertChain, String authType) throws CertificateException {
			if (!contextProvider.get().isTrustNegotiationMode()) {
				Utils.verifyCertChain(peerCertChain, contextProvider.get().getTrustedCerts());
			}
		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return new X509Certificate[0];
		}
	}
}
