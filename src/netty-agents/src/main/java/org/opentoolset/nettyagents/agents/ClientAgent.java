// ---
// Copyright 2020 netty-agents team
// All rights reserved
// ---
package org.opentoolset.nettyagents.agents;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslHandler;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import javax.net.ssl.KeyManagerFactory;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.opentoolset.nettyagents.AbstractAgent;
import org.opentoolset.nettyagents.AbstractMessage;
import org.opentoolset.nettyagents.AbstractRequest;
import org.opentoolset.nettyagents.Constants;
import org.opentoolset.nettyagents.Context;
import org.opentoolset.nettyagents.InboundMessageHandler;
import org.opentoolset.nettyagents.MessageDecoder;
import org.opentoolset.nettyagents.MessageEncoder;
import org.opentoolset.nettyagents.PeerContext;
import org.opentoolset.nettyagents.Utils;
import org.opentoolset.nettyagents.messages.ClientIdentity.GetOrSetIdRequest;
import org.opentoolset.nettyagents.messages.ClientIdentity.GetOrSetIdResponse;

/**
 * Client Agent is a type of agent which makes connection attempts to a server-peer, and maintain communication with its peer.
 *
 * @author hadi
 */
@Slf4j
public class ClientAgent extends AbstractAgent {

	private static final String DEFAULT_IN_MEMORY_KEYSTORE_PW = "";

	private EventLoopGroup workerGroup = new NioEventLoopGroup();
	private Bootstrap bootstrap = new Bootstrap();

	private Config config = Config.builder().build();

	private PeerContext server = new PeerContext();

	private boolean shutdownRequested = false;

	// ---

	/**
	 * Configuration object including configuration parameters for this agent.<br />
	 * Configuration parameters can be changed if needed. <br />
	 * All configuration adjustments should be made before calling the method "startup".
	 *
	 * @return Configuration object
	 */
	public Config getConfig() {
		return config;
	}

	/**
	 * Returns the oject defining the context of the peer-server.
	 *
	 * @return
	 */
	public PeerContext getServer() {
		return server;
	}

	@Override
	public void stopPeerIdentificationMode() {
		super.stopPeerIdentificationMode();
		if (!this.server.isTrusted()) {
			this.server.getChannelHandlerContext().close();
			this.server.setChannelHandlerContext(null);
		}
	}

	@Override
	public void startup() {
		super.startup();

		this.shutdownRequested = false;

		buildSSLContextIfEnabled();

		this.bootstrap.group(workerGroup);
		this.bootstrap.channel(NioSocketChannel.class);
		this.bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
		this.bootstrap.handler(new ClientChannelInitializer());
		this.bootstrap.remoteAddress(new InetSocketAddress(config.getRemoteHost(), config.getRemotePort()));

		setRequestHandler(GetOrSetIdRequest.class, request -> {
			if (StringUtils.isBlank(config.getId())) {
				config.setId(request.getId());
			}

			return new GetOrSetIdResponse(config.getId());
		});

		new Thread(() -> maintainConnection()).start();
	}

	public void shutdown() {
		try {
			this.shutdownRequested = true;

			this.workerGroup.shutdownGracefully();

			ChannelHandlerContext channelHandlerContext = this.server.getChannelHandlerContext();
			if (channelHandlerContext != null) {
				channelHandlerContext.close();
			}
		} catch (Exception e) {
			log.warn(e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Sends a request to the server and waits until receiving the response
	 *
	 * @param <I>
	 * @param <O>
	 * @param request
	 * @return
	 */
	public <I extends AbstractRequest<O>, O extends AbstractMessage> O doRequest(I request) {
		return getContext().getMessageSender().doRequest(request, this.server);
	}

	/**
	 * Sends a request to the server and waits until receiving the response or reaching to the specified timeout duration
	 *
	 * @param <I>
	 * @param <O>
	 * @param request
	 * @param timeoutSec
	 * @return
	 */
	public <I extends AbstractRequest<O>, O extends AbstractMessage> O doRequest(I request, int timeoutSec) {
		return getContext().getMessageSender().doRequest(request, this.server, timeoutSec);
	}

	/**
	 * Sends a request to the server in async mode and call observer when response is ready
	 *
	 * @param request
	 * @param observer
	 * @param <I>
	 * @param <O>
	 */
	public <I extends AbstractRequest<O>, O extends AbstractMessage> void doRequestAsync(I request, Consumer<O> observer) {
		getContext().getMessageSender().doRequestAsync(request, this.server, observer);
	}

	/**
	 * Sends a request to the server in async mode and returns a Future to get the results when needed
	 *
	 * @param request
	 * @param <O>
	 * @param <I>
	 * @return Future
	 */
	public <O extends AbstractRequest<I>, I extends AbstractMessage> Future<I> doRequestAsync(O request) {
		return getContext().getMessageSender().doRequestAsync(request, this.server);
	}

	/**
	 * Sends a message to the server without waiting a response
	 *
	 * @param message
	 */
	public void sendMessage(AbstractMessage message) {
		getContext().getMessageSender().sendMessage(message, this.server);
	}

	// ---

	private void maintainConnection() {
		while (!this.shutdownRequested) {
			try {
				ChannelFuture channelFuture = null;
				while (!this.shutdownRequested && (channelFuture = connectSafe()) == null) {
					TimeUnit.SECONDS.sleep(1);
				}

				if (channelFuture != null) {
					channelFuture.channel().closeFuture().sync();
				}
			} catch (InterruptedException e) {
				log.error(e.getLocalizedMessage(), e);
				Thread.currentThread().interrupt();
			}
		}
	}

	private ChannelFuture connectSafe() throws InterruptedException {
		try {
			if (!this.shutdownRequested) {
				return this.bootstrap.connect().sync();
			}
		} catch (InterruptedException e) {
			log.debug(e.getLocalizedMessage(), e);
			Thread.currentThread().interrupt();
		}

		return null;
	}

	private void buildSSLContextIfEnabled() {
		if (getConfig().getTlsConfig().isEnabled()) {
			try {
				KeyStore keystore = KeyStore.getInstance("JKS");
				PrivateKey key = getConfig().getTlsConfig().getPriKey();
				Certificate cert = getConfig().getTlsConfig().getCert();

				keystore.load(null);
				keystore.setCertificateEntry("my-cert", cert);
				keystore.setKeyEntry("my-key", key, DEFAULT_IN_MEMORY_KEYSTORE_PW.toCharArray(), new Certificate[]{cert});

				KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
				keyManagerFactory.init(keystore, DEFAULT_IN_MEMORY_KEYSTORE_PW.toCharArray());

				SslContextBuilder builder = SslContextBuilder.forClient();
				builder.keyManager(keyManagerFactory);
				builder.trustManager(new TrustManager(() -> getContext()));
				SslContext sslContext = builder.build();
				setSslContext(sslContext);
			} catch (IOException | GeneralSecurityException e) {
				log.error(e.getLocalizedMessage(), e);
			}
		}
	}

	// ---

	@Getter
	@Setter
	@Builder
	public static class Config extends AbstractConfig {

		private String id;

		@Default
		private String remoteHost = Constants.DEFAULT_SERVER_HOST;

		@Default
		private int remotePort = Constants.DEFAULT_SERVER_PORT;
	}

	// ---

	private final class ClientChannelInitializer extends ChannelInitializer<SocketChannel> implements InboundMessageHandler.Provider {

		private SslHandler sslHandler;

		@Override
		public void initChannel(SocketChannel channel) throws Exception {
			try {
				ChannelPipeline pipeline = channel.pipeline();

				SslContext sslContext = getSslContext();
				if (sslContext != null) {
					this.sslHandler = sslContext.newHandler(channel.alloc(), ClientAgent.this.getConfig().getRemoteHost(),
															ClientAgent.this.getConfig().getRemotePort());
					this.sslHandler.setHandshakeTimeout(Constants.DEFAULT_TLS_HANDSHAKE_TIMEOUT_SEC, TimeUnit.SECONDS);
					pipeline.addLast(this.sslHandler);
				}

				pipeline.addLast(new MessageEncoder(), new MessageDecoder(), new InboundMessageHandler(this));
				pipeline.addLast(new ClientChannelHandler(this.sslHandler));
			} catch (Exception e) {
				log.debug(e.getLocalizedMessage(), e);
			}
		}

		@Override
		public AbstractConfig getConfig() {
			return ClientAgent.this.getConfig();
		}

		@Override
		public Context getContext() {
			return ClientAgent.this.getContext();
		}

		@Override
		public boolean verifyChannelHandlerContext(ChannelHandlerContext ctx) {
			return getContext().isTrustNegotiationMode() || Utils.verifyChannelHandlerContext(ctx, ClientAgent.this.server);
		}
	}

	private final class ClientChannelHandler implements ChannelHandler {

		private SslHandler sslHandler;

		public ClientChannelHandler(SslHandler sslHandler) {
			this.sslHandler = sslHandler;
		}

		@Override
		public void handlerAdded(ChannelHandlerContext ctx) {
			if (getConfig().getTlsConfig().isEnabled()) {
				sslHandler.handshakeFuture().addListener(future -> onHandshakeCompleted(ctx));
			} else {
				ClientAgent.this.server.setChannelHandlerContext(ctx);
			}
		}

		@Override
		public void handlerRemoved(ChannelHandlerContext ctx) {
			ctx.close();
			ClientAgent.this.server.setChannelHandlerContext(null);
		}

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
			// No implementation is required
		}

		private void onHandshakeCompleted(ChannelHandlerContext ctx) {
			try {
				Certificate[] peerCerts = this.sslHandler.engine().getSession().getPeerCertificates();
				if (!getContext().isTrustNegotiationMode()) {
					Utils.verifyCertChain(peerCerts, getContext().getTrustedCerts());
				}

				Certificate peerCert = peerCerts[0];
				if (peerCert instanceof X509Certificate x509Certificate) {
					server.setCert(x509Certificate);
					server.setChannelHandlerContext(ctx);
					if (!getContext().isTrustNegotiationMode()) {
						server.setTrusted(true);
					}
				}
			} catch (Exception e) {
				log.debug(e.getLocalizedMessage(), e);
			}
		}
	}
}
