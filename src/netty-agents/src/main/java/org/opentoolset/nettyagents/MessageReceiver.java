// ---
// Copyright 2020 netty-agents team
// All rights reserved
// ---
package org.opentoolset.nettyagents;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MessageReceiver {

	private Map<Class<? extends AbstractMessage>, Function<AbstractRequest<?>, AbstractMessage>> requestHandlers = new HashMap<>();

	private Map<Class<? extends AbstractMessage>, Consumer<AbstractMessage>> messageHandlers = new HashMap<>();

	// ---

	@SuppressWarnings("unchecked")
	public <I extends AbstractRequest<O>, O extends AbstractMessage> void setRequestHandler(Class<I> classOfRequest, Function<I, O> function) {
		requestHandlers.put(classOfRequest, (Function<AbstractRequest<?>, AbstractMessage>) function);
	}

	@SuppressWarnings("unchecked")
	public <T extends AbstractMessage> void setMessageHandler(Class<T> classOfMessage, Consumer<T> consumer) {
		messageHandlers.put(classOfMessage, (Consumer<AbstractMessage>) consumer);
	}

	// ---

	AbstractMessage handleRequest(MessageWrapper messageWrapper) {
		AbstractMessage message = messageWrapper.deserializeMessage();
		if (message == null) {
			log.warn("Request message is null");
			return null;
		}

		if (!(AbstractRequest.class.isAssignableFrom(message.getClass()))) {
			log.warn("Request message is not a request. It's class: {}", message.getClass());
			return null;
		}

		AbstractRequest<?> request = (AbstractRequest<?>) message;
		Function<AbstractRequest<?>, AbstractMessage> function = requestHandlers.get(request.getClass());
		if (function == null) {
			log.warn("Unsupported operation for request class: {}", request.getClass());
			return null;
		}

		return function.apply(request);
	}

	void handleMessage(MessageWrapper messageWrapper) {
		AbstractMessage message = messageWrapper.deserializeMessage();
		if (message == null) {
			log.warn("Request message is null");
			return;
		}

		Consumer<AbstractMessage> consumer = messageHandlers.get(message.getClass());
		if (consumer == null) {
			log.warn("Unsupported operation for message class: {}", message.getClass());
			return;
		}

		consumer.accept(message);
	}
}
