package org.opentoolset.nettyagents;

import java.io.Serializable;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SerializationException;
import org.apache.commons.lang3.SerializationUtils;
import org.opentoolset.nettyagents.MessageWrapper.Serializer;

@Deprecated(forRemoval = true)
@Slf4j
public class SerializerJava implements Serializer {

	@Override
	public String serialize(Object object) {
		if (object instanceof Serializable serializable) {
			byte[] serialized = SerializationUtils.serialize(serializable);
			return new String(serialized, Constants.DEFAULT_CHARSET);
		} else {
			log.error(String.format("Object is not serializable, object: %s", object), new SerializationException());
			return null;
		}
	}

	@Override
	public <T> T deserialize(String serialized, Class<T> classOfObj) {
		try {
			Object object = SerializationUtils.deserialize(serialized.getBytes(Constants.DEFAULT_CHARSET));
			if (classOfObj.isInstance(object)) {
				@SuppressWarnings("unchecked")
				T message = (T) object;
				return message;
			}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}

		return null;
	}
}
