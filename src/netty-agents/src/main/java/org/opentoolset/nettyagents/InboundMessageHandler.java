// ---
// Copyright 2020 netty-agents team
// All rights reserved
// ---
package org.opentoolset.nettyagents;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;
import org.opentoolset.nettyagents.AbstractAgent.AbstractConfig;
import org.opentoolset.nettyagents.MessageSender.OperationContext;

@Slf4j
public class InboundMessageHandler extends ChannelInboundHandlerAdapter {

	private Provider provider;

	public interface Provider {

		AbstractConfig getConfig();

		Context getContext();

		boolean verifyChannelHandlerContext(ChannelHandlerContext ctx);
	}

	// ---

	public InboundMessageHandler(Provider provider) {
		this.provider = provider;
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		Class<?> type = msg.getClass();
		if (msg instanceof MessageWrapper messageWrapper) {
			type = messageWrapper.getClassOfMessage();
		}

		if (this.provider.getConfig().getTlsConfig().isEnabled() && !this.provider.verifyChannelHandlerContext(ctx)) {
			log.warn("The incoming message is not coming from a verified channel, so it will be ignored. Remote address: {}, Message type:{}",
					 ctx.channel().remoteAddress(), type.getName());

			return;
		}

		if (msg instanceof MessageWrapper messageWrapper) {
			String correlationId = messageWrapper.getCorrelationId();
			if (correlationId != null) {
				OperationContext operationContext = this.provider.getContext().getMessageSender().getWaitingRequests().get(correlationId);
				if (operationContext != null) {
					operationContext.setResponseWrapper(messageWrapper);
					Thread thread = operationContext.getThread();
					synchronized (thread) {
						if (thread.isAlive()) {
							thread.notifyAll();
						}
					}
				} else {
					log.warn("Response was ignored because of timeout");
				}
			} else {
				new Thread(() -> {
					String id = messageWrapper.getId();
					if (id != null) {
						AbstractMessage response = this.provider.getContext().getMessageReceiver().handleRequest(messageWrapper);
						MessageWrapper responseWrapper = MessageWrapper.createResponse(response, id);
						ctx.writeAndFlush(responseWrapper);
					} else {
						this.provider.getContext().getMessageReceiver().handleMessage(messageWrapper);
					}
				}).start();
			}
		} else {
			log.warn("Message couldn't be recognized");
		}
	}
}