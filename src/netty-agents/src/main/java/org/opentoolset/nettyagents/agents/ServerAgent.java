// ---
// Copyright 2020 netty-agents team
// All rights reserved
// ---
package org.opentoolset.nettyagents.agents;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.ssl.ClientAuth;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslHandler;
import java.io.IOException;
import java.net.SocketAddress;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.opentoolset.nettyagents.AbstractAgent;
import org.opentoolset.nettyagents.AbstractMessage;
import org.opentoolset.nettyagents.AbstractRequest;
import org.opentoolset.nettyagents.Constants;
import org.opentoolset.nettyagents.Context;
import org.opentoolset.nettyagents.InboundMessageHandler;
import org.opentoolset.nettyagents.MessageDecoder;
import org.opentoolset.nettyagents.MessageEncoder;
import org.opentoolset.nettyagents.PeerContext;
import org.opentoolset.nettyagents.Utils;
import org.opentoolset.nettyagents.messages.ClientIdentity.GetOrSetIdRequest;

/**
 * Server Agent is a type of agent which makes listens incoming connection requests to this agent, and maintain communication with each connected peer.
 *
 * @author hadi
 */
@Slf4j
public class ServerAgent extends AbstractAgent {

	private EventLoopGroup bossGroup = new NioEventLoopGroup();
	private EventLoopGroup workerGroup = new NioEventLoopGroup();
	private ServerBootstrap bootstrap = new ServerBootstrap();

	private Config config = Config.builder().build();

	/**
	 * Returns the map of clients with socket address as the key and PeerContext as the value
	 *
	 * @return map of clients
	 */
	@Getter
	private Map<SocketAddress, PeerContext> clients = new ConcurrentHashMap<>();

	private boolean shutdownRequested = false;

	// ---

	@Override
	public Config getConfig() {
		return config;
	}

	/**
	 * Returns map of clients with ID as the key and PeerContext as the value
	 *
	 * @return map of clients
	 */
	public Map<String, PeerContext> getClientsById() {
		return clients.values().stream().collect(Collectors.toMap(PeerContext::getId, Function.identity()));
	}

	@Override
	public void stopPeerIdentificationMode() {
		super.stopPeerIdentificationMode();

		for (Entry<SocketAddress, PeerContext> entry : new ArrayList<>(clients.entrySet())) {
			SocketAddress key = entry.getKey();
			PeerContext client = entry.getValue();
			if (!client.isTrusted()) {
				clients.remove(key);
				client.getChannelHandlerContext().close();
				client.setChannelHandlerContext(null);
			}
		}
	}

	@Override
	public void startup() {
		super.startup();

		this.shutdownRequested = false;

		buildSSLContextIfEnabled();

		this.bootstrap.group(this.bossGroup, this.workerGroup);
		this.bootstrap.channel(NioServerSocketChannel.class);
		this.bootstrap.option(ChannelOption.SO_BACKLOG, 128);
		this.bootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);
		this.bootstrap.localAddress(this.config.getLocalHost(), this.config.getLocalPort());
		this.bootstrap.childHandler(new ServerChannelInitializer());
		new Thread(() -> maintainConnection()).start();
	}

	public void shutdown() {
		this.shutdownRequested = true;

		this.bossGroup.shutdownGracefully();
		this.workerGroup.shutdownGracefully();

		getContext().getMessageSender().shutdown();
	}

	public <I extends AbstractRequest<O>, O extends AbstractMessage> O doRequest(I request, PeerContext peerContext) {
		return getContext().getMessageSender().doRequest(request, peerContext);
	}

	public <I extends AbstractRequest<O>, O extends AbstractMessage> O doRequest(I request, PeerContext peerContext, int timeoutSec) {
		return getContext().getMessageSender().doRequest(request, peerContext, timeoutSec);
	}

	public <I extends AbstractRequest<O>, O extends AbstractMessage> void doRequestAsync(I request, PeerContext peerContext, Consumer<O> observer) {
		getContext().getMessageSender().doRequestAsync(request, peerContext, observer);
	}

	public <I extends AbstractRequest<O>, O extends AbstractMessage> Future<O> doRequestAsync(I request, PeerContext peerContext) {
		return getContext().getMessageSender().doRequestAsync(request, peerContext);
	}

	public void sendMessage(AbstractMessage message, PeerContext peerContext) {
		getContext().getMessageSender().sendMessage(message, peerContext);
	}

	// ---

	private void maintainConnection() {
		try {
			while (!this.shutdownRequested) {
				ChannelFuture channelFuture = this.bootstrap.bind(this.config.getLocalPort()).sync();
				channelFuture.channel().closeFuture().sync();
			}
		} catch (InterruptedException e) {
			log.error(e.getLocalizedMessage(), e);
			Thread.currentThread().interrupt();
		}
	}

	private void buildSSLContextIfEnabled() {
		if (getConfig().getTlsConfig().isEnabled()) {
			try {
				PrivateKey key = getConfig().getTlsConfig().getPriKey();
				X509Certificate cert = getConfig().getTlsConfig().getCert();

				SslContextBuilder builder = SslContextBuilder.forServer(key, cert);
				builder.trustManager(new TrustManager(() -> getContext()));
				builder.clientAuth(ClientAuth.REQUIRE);
				SslContext sslContext = builder.build();
				setSslContext(sslContext);
			} catch (IOException e) {
				log.error(e.getLocalizedMessage(), e);
			}
		}
	}

	// ---

	@Setter
	@Getter
	@Builder
	public static class Config extends AbstractConfig {

		@Default
		private String localHost = Constants.DEFAULT_SERVER_HOST;

		@Default
		private int localPort = Constants.DEFAULT_SERVER_PORT;
	}

	// ---

	private final class ServerChannelInitializer extends ChannelInitializer<SocketChannel> implements InboundMessageHandler.Provider {

		private SslHandler sslHandler;

		@Override
		public void initChannel(SocketChannel channel) throws Exception {
			try {
				ChannelPipeline pipeline = channel.pipeline();

				SslContext sslContext = getSslContext();
				if (sslContext != null) {
					this.sslHandler = sslContext.newHandler(channel.alloc());
					this.sslHandler.setHandshakeTimeout(Constants.DEFAULT_TLS_HANDSHAKE_TIMEOUT_SEC, TimeUnit.SECONDS);
					pipeline.addLast(this.sslHandler);
				}

				pipeline.addLast(new MessageEncoder(), new MessageDecoder(), new InboundMessageHandler(this));
				pipeline.addLast(new ServerChannelHandler(this.sslHandler));
			} catch (Exception e) {
				log.debug(e.getLocalizedMessage(), e);
			}
		}

		@Override
		public AbstractConfig getConfig() {
			return ServerAgent.this.getConfig();
		}

		@Override
		public Context getContext() {
			return ServerAgent.this.getContext();
		}

		@Override
		public boolean verifyChannelHandlerContext(ChannelHandlerContext ctx) {
			return getContext().isTrustNegotiationMode()
				|| ServerAgent.this.clients.values().stream().anyMatch(client -> Utils.verifyChannelHandlerContext(ctx, client));
		}
	}

	private final class ServerChannelHandler implements ChannelHandler {

		private SslHandler sslHandler;

		public ServerChannelHandler(SslHandler sslHandler) {
			this.sslHandler = sslHandler;
		}

		@Override
		public void handlerAdded(ChannelHandlerContext ctx) {
			SocketAddress remoteAddress = ctx.channel().remoteAddress();

			if (this.sslHandler != null) {
				this.sslHandler.handshakeFuture().addListener(future -> onHandshakeCompleted(ctx, remoteAddress));
			} else {
				addOrUpdateClient(remoteAddress, ctx, null);
			}
		}

		@Override
		public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
			SocketAddress remoteAddress = ctx.channel().remoteAddress();
			ServerAgent.this.clients.remove(remoteAddress);
		}

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
			// No implementation is required
		}

		private void onHandshakeCompleted(ChannelHandlerContext ctx, SocketAddress remoteAddress) {
			try {
				Certificate[] peerCerts = this.sslHandler.engine().getSession().getPeerCertificates();
				Context context = getContext();
				if (!context.isTrustNegotiationMode()) {
					Utils.verifyCertChain(peerCerts, context.getTrustedCerts());
				}

				Certificate peerCert = peerCerts[0];
				if (peerCert instanceof X509Certificate x509Certificate) {
					addOrUpdateClient(remoteAddress, ctx, x509Certificate);
				}
			} catch (Exception e) {
				log.debug(e.getLocalizedMessage(), e);
			}
		}

		private void addOrUpdateClient(SocketAddress remoteAddress, ChannelHandlerContext channelHandlerContext, X509Certificate peerCert) {
			PeerContext peerContext = getOrCreatePeerContext(remoteAddress);
			peerContext.setChannelHandlerContext(channelHandlerContext);
			peerContext.setCert(peerCert);
			ServerAgent.this.clients.put(remoteAddress, peerContext);
			if (!getContext().isTrustNegotiationMode()) {
				peerContext.setTrusted(true);
				doRequestAsync(new GetOrSetIdRequest(UUID.randomUUID().toString()), peerContext, response -> peerContext.setId(response.getId()));
			}
		}

		private PeerContext getOrCreatePeerContext(SocketAddress remoteAddress) {
			PeerContext peerContext = ServerAgent.this.clients.get(remoteAddress);
			if (peerContext == null) {
				peerContext = new PeerContext();
			}

			return peerContext;
		}
	}
}