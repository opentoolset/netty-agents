package org.opentoolset.nettyagents.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.opentoolset.nettyagents.AbstractMessage;
import org.opentoolset.nettyagents.AbstractRequest;

public interface ClientIdentity {

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	class GetOrSetIdRequest extends AbstractRequest<GetOrSetIdResponse> {

		String id;
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	class GetOrSetIdResponse extends AbstractMessage {

		String id;
	}
}
